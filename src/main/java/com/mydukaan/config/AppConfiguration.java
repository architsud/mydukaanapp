
package com.mydukaan.config;

/**
 * @author Yogesh Verma
 *
 */

import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan("com.mydukaan")
@EnableJpaRepositories("com.mydukaan.repository")
@EnableTransactionManagement
public class AppConfiguration extends WebMvcConfigurerAdapter
{
    // configuring View Resolver
	@Bean //A bean is an object that is instantiated, assembled, and otherwise managed by a Spring IoC container. 
	public InternalResourceViewResolver viewResolver()
	{
	   InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	   viewResolver.setPrefix("/");
	   viewResolver.setSuffix(".jsp");
	   return viewResolver;
	}
	
	// configuring datasource
	@Bean //A bean is an object that is instantiated, assembled, and otherwise managed by a Spring IoC container. 
	public BasicDataSource dataSource()
	{
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/mydukaan");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}		
	
	// registering multipart bean
	@Bean
	public MultipartResolver multipartResolver(){
		CommonsMultipartResolver multipartResolver= new CommonsMultipartResolver();
		multipartResolver.setDefaultEncoding("utf-8");
		return multipartResolver;
	}
	
	// configuring EntityFactoryManager
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory()
	{
		JpaVendorAdapter vendorAdapter =new HibernateJpaVendorAdapter();
		
		LocalContainerEntityManagerFactoryBean factory= new LocalContainerEntityManagerFactoryBean();
		
		Properties props = new Properties();
		props.setProperty("hibernate.hbm2ddl.auto", "update");
		props.setProperty("hibernate.show_sql", "true");
		props.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		
		factory.setDataSource(dataSource());
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("com.mydukaan.model");
		factory.afterPropertiesSet();
		factory.setJpaProperties(props);
		return factory;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager(){
		JpaTransactionManager transactionManager= new JpaTransactionManager();
		transactionManager.setDataSource(dataSource());
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}
}

