/**
 * 
 */
package com.mydukaan.form;


/**
 * @author Yogesh Verma
 *
 */
public class OrderForm {
	
	private String id;
	
	private String productId;
	
	private String quantity;
	
	private String price;
	
	private String orderedOn;
	
	private String orderedBy;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the quantity
	 */
	public String getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the orderedOn
	 */
	public String getOrderedOn() {
		return orderedOn;
	}

	/**
	 * @param orderedOn the orderedOn to set
	 */
	public void setOrderedOn(String orderedOn) {
		this.orderedOn = orderedOn;
	}

	/**
	 * @return the orderedBy
	 */
	public String getOrderedBy() {
		return orderedBy;
	}

	/**
	 * @param orderedBy the orderedBy to set
	 */
	public void setOrderedBy(String orderedBy) {
		this.orderedBy = orderedBy;
	}
	
}
