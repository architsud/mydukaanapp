/**
 * 
 */
package com.mydukaan.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mydukaan.model.Order;

/**
 * @author Yogesh Verma
 *
 */
@Repository
public interface OrdersRepository extends  JpaRepository<Order, Serializable>{
	
}
