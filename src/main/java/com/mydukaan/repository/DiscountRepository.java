/**
 * 
 */
package com.mydukaan.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mydukaan.model.Discount;

/**
 * @author Yogesh Verma
 *
 */
public interface DiscountRepository extends JpaRepository<Discount, Serializable>{

}
