/**
 * 
 */
package com.mydukaan.common;

/**
 * @author Yogesh Verma
 *
 */
public class Constants {

	public static final int Integer_ZERO=0;
	
	public static final String ERROR="error";
	
	public static final String SUCCESS="success";
}
