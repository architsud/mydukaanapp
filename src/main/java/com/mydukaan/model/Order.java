/**
 * 
 */
package com.mydukaan.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Yogesh Verma
 *
 */

@Entity
@Table(name="orders")
public class Order {
	
	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="product_id")
	private String productId;
	
	@Column(name="quantity")
	private Integer quantity;
	
	@Column(name="price")
	private Integer price;
	
	@Column(name="ordered_on")
	private Date orderedOn;
	
	@Column(name="ordered_by")
	private String orderedBy;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the price
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

	/**
	 * @return the orderedOn
	 */
	public Date getOrderedOn() {
		return orderedOn;
	}

	/**
	 * @param orderedOn the orderedOn to set
	 */
	public void setOrderedOn(Date orderedOn) {
		this.orderedOn = orderedOn;
	}

	/**
	 * @return the orderedBy
	 */
	public String getOrderedBy() {
		return orderedBy;
	}

	/**
	 * @param orderedBy the orderedBy to set
	 */
	public void setOrderedBy(String orderedBy) {
		this.orderedBy = orderedBy;
	}
	
}
