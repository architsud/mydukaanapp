/**
 * 
 */
package com.mydukaan.util;

import java.util.List;

import com.mydukaan.form.DiscountForm;
import com.mydukaan.form.OrderForm;

/**
 * @author Yogesh Verma
 *
 */
public class SuccessOrErrorMessage {
	
	private String successOrError;
	
	private String message;
	
	private List<OrderForm> orderForms;
	
	private List<DiscountForm>  discountForms;
	
	/**
	 * @return the successOrError
	 */
	public String getSuccessOrError() {
		return successOrError;
	}
	/**
	 * @param successOrError the successOrError to set
	 */
	public void setSuccessOrError(String successOrError) {
		this.successOrError = successOrError;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the orderForms
	 */
	public List<OrderForm> getOrderForms() {
		return orderForms;
	}
	/**
	 * @param orderForms the orderForms to set
	 */
	public void setOrderForms(List<OrderForm> orderForms) {
		this.orderForms = orderForms;
	}
	/**
	 * @return the discountForms
	 */
	public List<DiscountForm> getDiscountForms() {
		return discountForms;
	}
	/**
	 * @param discountForms the discountForms to set
	 */
	public void setDiscountForms(List<DiscountForm> discountForms) {
		this.discountForms = discountForms;
	}
	
}
