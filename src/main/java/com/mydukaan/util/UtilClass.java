/**
 * 
 */
package com.mydukaan.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/**
 * @author Yogesh Verma
 *
 */
public class UtilClass {
	
	/**
	 * Method to check if String is not empty and not null.
	 * @param arg
	 * @return Boolean
	 */
	public static Boolean isStringNotEmptyAndNotNull(String arg) {
		if (arg != null && !("").equals(arg) && !("").equals(arg.trim())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Method to check if a List/Set is not null and not empty.
	 * @param c
	 * @return Boolean
	 */
	public static Boolean isCollectionNotEmtyAndNotNull(Collection<?> arg) {
		if (arg != null && !arg.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Method to check if a Date is not 0 and not empty.
	 * @param c
	 * @return
	 */
	public static Boolean isDateNotEmtyAndNotNull(Date arg) {
		if (arg != null && !("").equals(arg)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Method to format date in "dd-MM-yyyy"
	 * @param dateStr as String
	 * @return formatted date
	 */
	public static Date parseDateDDMMYYYYFormat(String dateStr) {
		SimpleDateFormat format = null;
		Date date = null;
		try {
			format = new SimpleDateFormat("dd-MM-yyyy");
			date = format.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	/**
	 * Method to get date in string 
	 * @param date
	 * @return
	 */
	public static String parseDateToStringDDMMYYYY(Date date) {
		SimpleDateFormat format = null;
		String dateStr = null;
		format = new SimpleDateFormat("dd-MMM-yyyy");
		dateStr = format.format(date);
		return dateStr;
	}
}
