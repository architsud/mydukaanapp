/**
 * 
 */
package com.mydukaan.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mydukaan.form.DiscountForm;
import com.mydukaan.form.OrderForm;

/**
 * @author Yogesh Verma
 *
 */

public interface ReadCSV {
	
	public List<OrderForm> readOrderCSV(MultipartFile file) throws ParseException, IOException;
	
	public List<DiscountForm> readDiscountCSV(MultipartFile file) throws ParseException, IOException;
}
