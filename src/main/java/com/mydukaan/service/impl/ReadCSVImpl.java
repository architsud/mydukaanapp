/**
 * 
 */
package com.mydukaan.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mydukaan.common.Constants;
import com.mydukaan.form.DiscountForm;
import com.mydukaan.form.OrderForm;
import com.mydukaan.model.Discount;
import com.mydukaan.model.Order;
import com.mydukaan.repository.DiscountRepository;
import com.mydukaan.repository.OrdersRepository;
import com.mydukaan.service.ReadCSV;
import com.mydukaan.util.UtilClass;
import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;

/**
 * @author Yogesh Verma
 *
 */

@Service
public class ReadCSVImpl implements ReadCSV{

	
	@Autowired
	OrdersRepository orderDAO;
	
	@Autowired
	DiscountRepository discountDAO;
	
	/* (non-Javadoc)
	 * @see com.mydukaan.service.ReadCSV#readOrderCSV(org.springframework.web.multipart.MultipartFile)
	 */
	@Transactional
	public List<OrderForm> readOrderCSV(MultipartFile file) throws ParseException, IOException {
		//converting input stream into text
		BufferedReader bufferReader= new BufferedReader(new InputStreamReader(file.getInputStream()));
		//Instantiating open CSV
		CSVReader csvBuilder = new CSVReader(bufferReader, ',');
        ColumnPositionMappingStrategy<OrderForm> beanStrategy = new ColumnPositionMappingStrategy<OrderForm>();
        beanStrategy.setType(OrderForm.class);
        //mapping csv file column with open csv
        beanStrategy.setColumnMapping(new String[] {"id","productId","Quantity","Price","orderedOn","orderedBy"});
        CsvToBean<OrderForm> csvToBean = new CsvToBean<OrderForm>();
        List<OrderForm> ordersFormList = csvToBean.parse(beanStrategy, csvBuilder);
        if(UtilClass.isCollectionNotEmtyAndNotNull(ordersFormList)){
        	//removing header from the csv file
        	ordersFormList.remove(Constants.Integer_ZERO);
	        //checking if csv file contains data (other than headers)
	        if(ordersFormList.size()> Constants.Integer_ZERO){
	        	List<Order> orderList = new ArrayList<Order>();
	        	orderList=convertOrderFormToModel(ordersFormList);
		        orderDAO.save(orderList);
	        }
        }
        return ordersFormList;
	}
	
	/**
	 * converts orderForm to order model
	 * @param ordersFormList
	 * @return
	 * @throws ParseException
	 */
	private List<Order> convertOrderFormToModel(List<OrderForm> ordersFormList) throws ParseException{
		List<Order> orders = new ArrayList<Order>();
		for(OrderForm orderForm:ordersFormList){
       	 Order order = new Order();
       	 if(UtilClass.isStringNotEmptyAndNotNull(orderForm.getId())){
       		 order.setId(Integer.parseInt(orderForm.getId()));
       	 }
       	if(UtilClass.isStringNotEmptyAndNotNull(orderForm.getPrice())){
       		order.setPrice(Integer.parseInt(orderForm.getPrice()));
      	 }
       	if(UtilClass.isStringNotEmptyAndNotNull(orderForm.getProductId())){
       		order.setProductId(orderForm.getProductId());
      	 }
       	if(UtilClass.isStringNotEmptyAndNotNull(orderForm.getQuantity())){
       		order.setQuantity(Integer.parseInt(orderForm.getQuantity()));
      	 }
       	if(UtilClass.isStringNotEmptyAndNotNull(orderForm.getOrderedOn())){
       		SimpleDateFormat sdf= new SimpleDateFormat("dd-MM-yyyy");
      		 order.setOrderedOn(sdf.parse(orderForm.getOrderedOn()));
      	 }
       	if(UtilClass.isStringNotEmptyAndNotNull(orderForm.getOrderedBy())){
       		order.setOrderedBy(orderForm.getOrderedBy());
      	 }
   		orders.add(order);
       }
		return orders;
	}
	
	/* (non-Javadoc)
	 * @see com.mydukaan.service.ReadCSV#readDiscountCSV(org.springframework.web.multipart.MultipartFile)
	 */
	@Transactional
	public List<DiscountForm> readDiscountCSV(MultipartFile file) throws ParseException, IOException {
		BufferedReader bufferReader= new BufferedReader(new InputStreamReader(file.getInputStream()));
		//Instantiating open CSV
		CSVReader csvBuilder = new CSVReader(bufferReader, ',');
        ColumnPositionMappingStrategy<DiscountForm> beanStrategy = new ColumnPositionMappingStrategy<DiscountForm>();
        beanStrategy.setType(DiscountForm.class);
        //mapping csv file column with open csv
        beanStrategy.setColumnMapping(new String[] {"id","amount","Quantity","productId","createdOn","createdBy"});
        CsvToBean<DiscountForm> csvToBean = new CsvToBean<DiscountForm>();
        List<DiscountForm> discountFormList = csvToBean.parse(beanStrategy, csvBuilder);
        if(UtilClass.isCollectionNotEmtyAndNotNull(discountFormList)){
        	//removing header from the csv file
        	discountFormList.remove(Constants.Integer_ZERO);
	        //checking if csv file contains data (other than headers)
	        if(discountFormList.size()> Constants.Integer_ZERO){
	        	List<Discount> discountList = new ArrayList<Discount>();
	        	discountList=convertdicountFormToModel(discountFormList);
	        	discountDAO.save(discountList);
	        }
        }
        return discountFormList;
	}
	
	
	/**
	 * converts discount form to discount model
	 * @param discountFormList
	 * @return
	 * @throws ParseException
	 */
	private List<Discount> convertdicountFormToModel(List<DiscountForm> discountFormList) throws ParseException{
		List<Discount> discountList = new ArrayList<Discount>();
		for(DiscountForm discountForm:discountFormList){
       	 Discount discount = new Discount();
       	 if(UtilClass.isStringNotEmptyAndNotNull(discountForm.getId())){
       		discount.setId(Integer.parseInt(discountForm.getId()));
       	 }
       	if(UtilClass.isStringNotEmptyAndNotNull(discountForm.getAmount())){
       		discount.setAmount(Integer.parseInt(discountForm.getAmount()));
      	 }
       	if(UtilClass.isStringNotEmptyAndNotNull(discountForm.getProductId())){
       		discount.setProductId(Integer.parseInt(discountForm.getProductId()));
      	 }
       	if(UtilClass.isStringNotEmptyAndNotNull(discountForm.getCreatedBy())){
       		discount.setCreatedB(discountForm.getCreatedBy());
      	 }
       	if(UtilClass.isStringNotEmptyAndNotNull(discountForm.getCreatedOn())){
       		SimpleDateFormat sdf= new SimpleDateFormat("dd-MM-yyyy");
       		discount.setCreatedOn((sdf.parse(discountForm.getCreatedOn())));
      	 }
       	discountList.add(discount);
       }
		return discountList;
	}
}
