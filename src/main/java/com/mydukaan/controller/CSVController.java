/**
 * 
 */
package com.mydukaan.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.mydukaan.common.Constants;
import com.mydukaan.form.DiscountForm;
import com.mydukaan.form.OrderForm;
import com.mydukaan.service.ReadCSV;
import com.mydukaan.util.SuccessOrErrorMessage;

/**
 * @author Yogesh Verma
 *
 */

@Controller
public class CSVController {
	
	
	@Autowired ReadCSV readCSV;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String viewUpload(){
		return "uploadCSV" ;
	}
		
	
	@RequestMapping(value="/order", method=RequestMethod.POST)
	public ResponseEntity<SuccessOrErrorMessage> readCSV(MultipartFile file){
		List<OrderForm> orderForms = new ArrayList<OrderForm>();
		if(!file.getOriginalFilename().isEmpty()){
			try {
				orderForms = readCSV.readOrderCSV(file);
			} catch (ParseException e) {
				e.printStackTrace();
				SuccessOrErrorMessage message= new SuccessOrErrorMessage();
				message.setMessage(e.getMessage());
				message.setSuccessOrError(Constants.ERROR);
				return new ResponseEntity<SuccessOrErrorMessage>(message,HttpStatus.INTERNAL_SERVER_ERROR);
			}catch (IOException e) {
				e.printStackTrace();
				SuccessOrErrorMessage message= new SuccessOrErrorMessage();
				message.setMessage(e.getMessage());
				message.setSuccessOrError(Constants.ERROR);
				return new ResponseEntity<SuccessOrErrorMessage>(message,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else{
			SuccessOrErrorMessage message= new SuccessOrErrorMessage();
			message.setMessage( "please upload a file");
			message.setSuccessOrError(Constants.ERROR);
			return new ResponseEntity<SuccessOrErrorMessage>(message, HttpStatus.NO_CONTENT);
		}
		SuccessOrErrorMessage message= new SuccessOrErrorMessage();
		message.setMessage("file uploaded successfully");
		message.setSuccessOrError(Constants.SUCCESS);
		message.setOrderForms(orderForms);
		return new ResponseEntity<SuccessOrErrorMessage>(message,HttpStatus.OK);
	}
	
	@RequestMapping(value="/discount", method=RequestMethod.POST)
	public ResponseEntity<SuccessOrErrorMessage> readDiscountCSV(MultipartFile file){
		List<DiscountForm> discountForms = new ArrayList<DiscountForm>();
		if(!file.getOriginalFilename().isEmpty()){
			try {
				discountForms = readCSV.readDiscountCSV(file);
			} catch (ParseException e) {
				e.printStackTrace();
				SuccessOrErrorMessage message= new SuccessOrErrorMessage();
				message.setMessage(e.getMessage());
				message.setSuccessOrError(Constants.ERROR);
				return new ResponseEntity<SuccessOrErrorMessage>(message,HttpStatus.INTERNAL_SERVER_ERROR);
			}catch (IOException e) {
				e.printStackTrace();
				SuccessOrErrorMessage message= new SuccessOrErrorMessage();
				message.setMessage(e.getMessage());
				message.setSuccessOrError(Constants.ERROR);
				return new ResponseEntity<SuccessOrErrorMessage>(message,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else{
			SuccessOrErrorMessage message= new SuccessOrErrorMessage();
			message.setMessage( "please upload a file");
			message.setSuccessOrError(Constants.ERROR);
			return new ResponseEntity<SuccessOrErrorMessage>(message, HttpStatus.NO_CONTENT);
		}
		SuccessOrErrorMessage message= new SuccessOrErrorMessage();
		message.setMessage("file uploaded successfully");
		message.setSuccessOrError(Constants.SUCCESS);
		message.setDiscountForms(discountForms);
		return new ResponseEntity<SuccessOrErrorMessage>(message,HttpStatus.OK);
	}
}
