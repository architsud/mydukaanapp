<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>DEMO FORM</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- <link rel="stylesheet" type="text/css" href="assets/css/styles.css"> -->
<style>
	.creditCardForm {
    max-width: 700px;
    background-color: #fff;
    margin: 100px auto;
    overflow: hidden;
    padding: 25px;
    color: #4c4e56;
}

.creditCardForm label {
    width: 100%;
    margin-bottom: 10px;
}

.creditCardForm .heading h1 {
    text-align: center;
    font-family: 'Open Sans', sans-serif;
    color: #4c4e56;
}

.creditCardForm .payment {
    float: left;
    font-size: 18px;
    padding: 10px 25px;
    margin-top: 20px;
    position: relative;
}

.creditCardForm .payment .form-group {
    float: left;
    margin-bottom: 15px;
}

.creditCardForm .payment .form-control {
    line-height: 46px;
    height: auto;
    padding: 0 16px;
}

.creditCardForm .owner {
    width: 63%;
    margin-right: 10px;
}

.creditCardForm .CVV {
    width: 35%;
}

.creditCardForm #card-number-field {
    width: 100%;
}

.creditCardForm #expiration-date {
    width: 49%;
}

.creditCardForm #credit_cards {
    width: 50%;
    margin-top: 25px;
    text-align: right;
}

.creditCardForm #pay-now {
    width: 100%;
    margin-top: 25px;
}

.creditCardForm .payment .btn {
    width: 100%;
    margin-top: 0px;
    font-size: 24px;
    background-color: #2ec4a5;
    color: white;
}

.creditCardForm .payment select {
    padding: 10px;
    margin-right: 15px;
}

.transparent {
    opacity: 0.2;
}

@media(max-width: 650px) {
    .creditCardForm .owner,
    .creditCardForm .CVV,
    .creditCardForm #expiration-date,
    .creditCardForm #credit_cards {
        width: 100%;
    }
    .creditCardForm #credit_cards {
        text-align: left;
    }
}


/*  Examples Section */

.examples {
	max-width: 700px;
	background-color: #fff;
	margin: 0 auto 75px;
	padding: 30px 50px;
	color: #4c4e56;
}

.examples-note{
    text-align: center;
    font-size: 14px;
    max-width: 370px;
    margin: 0 auto 40px;
    line-height: 1.7;
    color: #7a7a7a;
}

.examples table {
    margin: 5px 0 0 0;
    font-size: 14px;
}
</style>
<script>
function uploadOrderCSV(){
$("form#fileUploadForm").submit(function(e){
    e.preventDefault();
    var form = this;
    var formData = new FormData($(this)[0]);
    $.ajax({
       url: "csv",
       type: 'POST',
       data: formData,
       async: true,
       success: function (data, status, xhr) {
           console.log(data);
       },
       error: function(xhr, status, error) { 
           console.log(status);
       }, 
       cache: false,
       contentType: false,
       processData: false
   });
 });
}
</script>
</head>
<body>
<div class="container-fluid">
<div class="creditCardForm">
  <!-- <div class="heading">
    <h1>Main Heading Text</h1>
  </div> -->
  <div class="payment">
    <div class="container">
      <div class="col-md-6 col-xs-6">
      <form method="POST" enctype="multipart/form-data" id="fileUploadForm">
	        <div class="form-group">
	          <label class="control-label">Select a .CSV file</label>
	          <input type="file"  name="file" id="uploadOrder" class="filestyle" data-icon="false">
	        </div>
	        <div class="form-group" id="pay-now">
	          <button type="submit" class="btn btn-default" id="confirm-purchase" onclick="uploadOrderCSV()">Upload</button>
	        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- <div class="examples">
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Heading-01</th>
            <th>Heading-02</th>
            <th>Heading-02</th>
            <th>Heading-03</th>
            <th>Heading-04</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Content-01</td>
            <td>Content-02</td>
            <td>Content-03</td>
            <td>Content-04</td>
            <td>Content-05</td>
          </tr>
          <tr>
           <td>Content-01</td>
            <td>Content-02</td>
            <td>Content-03</td>
            <td>Content-04</td>
            <td>Content-05</td>
          </tr>
          <tr>
           <td>Content-01</td>
            <td>Content-02</td>
            <td>Content-03</td>
            <td>Content-04</td>
            <td>Content-05</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div> -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<script src="https://www.jquery-az.com/boots/js/bootstrap-filestyle.min.js"></script> 
</body>
</html>

